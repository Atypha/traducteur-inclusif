/**@var json trad  */
let trad = (await import('./locales/json/fr.json')).default;
window.addEventListener("load", function () {
    const form = document.getElementById("form");
    form.addEventListener("submit", inclusivation);
});

/**
 * MAJ le champ textApres par la valeur traduite.
 * @param string valeurAvant : le texte à traduire
 */
function inclusivation(event) {
    event.preventDefault();
    const champAvant = document.getElementById("textAvant");
    /**
     * @type HTMLTextAreaElement champApres
     */
    const champApres = document.getElementById("textApres");
    champApres.value = inclusivationage(champAvant.value, trad, 'fr');
    console.log("Traduction effectuée");
}

/**
 * Traduit un texte (de manière inclusive)
 * @param string textAvant : le texte à traduire
 * @returns string : le texte traduit
 */
function inclusivationage(textAvant, lang) {
    console.log(textAvant);
    let textApres = textAvant;

    const tableau = tokenizeText(textApres);
    textApres = tableau.map(mots => {
        return mots.map(function (mot) {
            if (mot === undefined) {
                return "";
            }
            if (trad[mot.toLowerCase()] === undefined) {
                return mot;
            }
            let newWord = trad[mot.toLowerCase()];
            if (mot[0]  === mot[0].toUpperCase()) {
                newWord = newWord[0][0].toUpperCase() + newWord[0].substring(1);
            }
            return newWord;
        }).join(" ");
    }).join("");

    return textApres;
}

/**
 * Tokenize a text into an array of words and punctuation marks
 * @param {string} text
 * @returns {string[][]}
 * @example
 * tokenizeText("Hello, world!") // [["Hello"], [","], ["world"], ["!"]]
 **/
function tokenizeText(text) {
    let tokens = [];
    let lastSplitIndex = 0;
    for (let i = 0; i < text.length; i++) {
        if ([",", ".", "!"].includes(text[i])) {
            const words = text.substring(lastSplitIndex, i).split(" ");
            tokens.push(words);
            tokens.push([text[i]]);
            lastSplitIndex = i + 1;
        }
    }

    return tokens;
}

//const str = new String("Original");

//To include è, é, à, ò, ì, just add them to the regex: [^\wèéàòìÈÉÀÒÌ]
// D:\Dev\Traducteur inclusif\Traducteur inclusif.js
//You might also use [^\d\p{Latin}], but that'll match more characters.
//d is for digits and \p{Latin} is a Unicode class for all Latin characters, including all diacritics
// ./node_modules/po2json/bin/po2jsonbin ./locales/po/fr.po ./locales/json/fr.json -p
